
// -------------------------------------------------------------------------- //

import { useTheme } from './Provider/ThemeContext';

// -------------------------------------------------------------------------- //

export const azulOpaco = '#3B526A';
export const negroOpaco = '#1C1C1E';
export const blancoOpaco = '#E3DDD3';

function Colores () {
    const { isDarkTheme } = useTheme();

    return {
        fondoDinamico: isDarkTheme ? blancoOpaco : negroOpaco,
        textoDinamico: isDarkTheme ? negroOpaco : blancoOpaco,
        
        itemDinamico: isDarkTheme ? azulOpaco : blancoOpaco,
        itemDinamicoTwo: isDarkTheme ? blancoOpaco : azulOpaco,
    };

};

export default Colores;

// -------------------------------------------------------------------------- //