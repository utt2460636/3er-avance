
// -------------------------------------------------------------------------- //

import React from 'react';
import Colores from './Colores';
import { Estilos } from './Estilos';
import { MAIN_URL } from '../Config';
import 'react-native-gesture-handler';
import LogoutButton from './Provider/Logout';
import ThemeButton from './Provider/ThemeButton';
import { useUser } from './Provider/UserProvider';
import { useTheme } from './Provider/ThemeContext';
import { MaterialIcons } from '@expo/vector-icons';
import { SafeAreaView } from 'react-native-safe-area-context';
import { View, ScrollView, Pressable, Image, Text  } from 'react-native';
import { createDrawerNavigator, DrawerItemList } from '@react-navigation/drawer';
import { NavigationContainer, useNavigation, DefaultTheme, DarkTheme } from '@react-navigation/native';

// Importación de las pantallas.
import HomeProvider from './Provider/HomeProvider';
import IniciarSesion from '../screens/IniciarSesion';
import ForgotPassword from '../screens/ForgotPassword';
import ChangePassword from '../screens/ChangePassword';

import Inicio from '../screens/Inicio';
import MIPerfil from '../screens/Profile/MiPerfil';
import EditarPerfil from '../screens/Profile/EditarPerfil';

import Editor from '../screens/Locker/Editor';


import AvailLockers from '../screens/AvailLockers';
import About from '../screens/About';
import QuestionsAndAnswers from '../screens/FAQ';

// -------------------------------------------------------------------------- //

const Drawer = createDrawerNavigator();

// Sección customizada para que se viese más bonita.
const VulturesHeader = ({ title }) => {
    const navigation = useNavigation();
    const themeColor = Colores();

    return (
        <SafeAreaView>
            <View style={[ Estilos.headerContainer, { backgroundColor: themeColor.fondoDinamico } ]}>

                <Pressable onPress={() => navigation.openDrawer()}>
                    <MaterialIcons name="menu" size={24} color={ themeColor.itemDinamico } />
                </Pressable>
                
                <Text style={[ Estilos.headerTitle, { color: themeColor.itemDinamico } ]}>{title}</Text>
                <ThemeButton />

            </View>
        </SafeAreaView>
    );
};


const Navegacion = () => {
    const { user } = useUser();
    const themeColor = Colores();
    const { isDarkTheme } = useTheme();
    const navigationTheme = isDarkTheme ? DarkTheme : DefaultTheme;

    const defaultImage = require('../assets/defaultProfile.png');
    const profileImageURL = user && user.FotoPerfil ? { uri: `${MAIN_URL}${user.FotoPerfil}` } : defaultImage;
    const profileName = user ? user.Nombres : 'LOCK-IT';

    // ---------------------------------------------------------------------- //

    return (
        <NavigationContainer theme={navigationTheme}>
            <Drawer.Navigator

                drawerContent = {(props) => {

                    return (
                        <SafeAreaView style={[ Estilos.safeContainer, { backgroundColor: themeColor.fondoDinamico } ]}>

                            <View style={Estilos.profileContainer}>
                                <Image source={profileImageURL} style={[Estilos.profilePicture, { borderColor: themeColor.itemDinamico }]} />
                                <Text style={[ Estilos.profileName, { color: themeColor.itemDinamico } ]}>{profileName}</Text>
                            </View>

                            <ScrollView style={Estilos.safeContainer} showsVerticalScrollIndicator={false} >
                                <DrawerItemList {...props} />
                            </ScrollView>

                            <View style={Estilos.logoutContainer}>
                                <LogoutButton />
                            </View>

                        </SafeAreaView>
                    );
                }}

                // ---------------------------------------------------------- //

                screenOptions = {{
                    headerTransparent: true,
                    drawerActiveTintColor: themeColor.fondoDinamico,
                    overlayColor: 'rgba(28, 28, 30, .6)',
                    drawerInactiveTintColor: themeColor.textoDinamico,
                    drawerLabelStyle: Estilos.drawerLabel,
                    drawerActiveBackgroundColor: themeColor.itemDinamico }}>


                <Drawer.Screen name='HomeAuth' component={HomeProvider} options={{
                    header: () => null, swipeEnabled: false, drawerItemStyle: { display: 'none' }
                }} />

                <Drawer.Screen name='Login' component={IniciarSesion} options={{
                    header: () => null, swipeEnabled: false, drawerItemStyle: { display: 'none' }
                }} />

                <Drawer.Screen name='ForgotPassword' component={ForgotPassword} options={{
                    header: () => null, swipeEnabled: false, drawerItemStyle: { display: 'none' }
                }} />

                <Drawer.Screen name='ChangePassword' component={ChangePassword} options={{
                    header: () => null, swipeEnabled: false, drawerItemStyle: { display: 'none' }
                }} />


                <Drawer.Screen name='Home' component={Inicio} options={{
                    title: 'Inicio', header: () => <VulturesHeader title="INICIO" />,
                }} />

                <Drawer.Screen name='Profile' component={MIPerfil} options={{
                    title: 'Mi Perfil', header: () => <VulturesHeader title="MI PERFIL" />,
                }} />

                <Drawer.Screen name='EditProfile' component={EditarPerfil} options={{
                    drawerItemStyle: { display: 'none' }, header: () => <VulturesHeader title="EDITAR PERFIL" />,
                }} />

                <Drawer.Screen name='EditLocker' component={Editor} options={{
                    title: 'Mi casillero', header: () => <VulturesHeader title="MI CASILLERO" />,
                }} />

                <Drawer.Screen name='AvailLockers' component={AvailLockers} options={{
                    title: 'Renta de Lockers', header: () => <VulturesHeader title="RENTAR CASILLERO" />,
                }} />

                <Drawer.Screen name='About' component={About} options={{
                    title: 'Información', header: () => <VulturesHeader title="INFORMACIÓN" />,
                }} />

                <Drawer.Screen name='FAQ' component={QuestionsAndAnswers} options={{
                    title: 'Dudas frecuentes', header: () => <VulturesHeader title="DUDAS FRECUENTES" />,
                }} />

            </Drawer.Navigator>
        </NavigationContainer>
    );

};

export default Navegacion;

// -------------------------------------------------------------------------- //