
// -------------------------------------------------------------------------- //

import React from 'react';
import Colores  from '../Colores';
import { Estilos } from '../Estilos';
import { Pressable, Text } from 'react-native';
import { useTheme } from './ThemeContext';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import Animated, { useSharedValue, useAnimatedStyle, withTiming } from 'react-native-reanimated';

// -------------------------------------------------------------------------- //

const ThemeButton = () => {
    const { isDarkTheme, setIsDarkTheme } = useTheme();
    const Rotacion = useSharedValue(0);

    // ---------------------------------------------------------------------- //

    const animatedStyles = useAnimatedStyle(() => {
        return { transform: [{ rotate: `${Rotacion.value}deg` }] };
    });

    const toggleTheme = () => {
        setIsDarkTheme(!isDarkTheme);
        Rotacion.value = withTiming(Rotacion.value + 360, { duration: 300 });
    };

    // ---------------------------------------------------------------------- //

    const themeColor = Colores();

    return (
        <Pressable onPress={toggleTheme}>
            <Animated.View style={[animatedStyles, Estilos.themeButton ]}>
                <MaterialCommunityIcons name={isDarkTheme ?  'weather-night' : 'weather-sunny' } size={24} color={ themeColor.itemDinamico } />
            </Animated.View>
        </Pressable>
    );
};

export default ThemeButton;

// -------------------------------------------------------------------------- //