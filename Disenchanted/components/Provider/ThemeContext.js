
// -------------------------------------------------------------------------- //

import { Appearance } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { createContext, useContext, useState, useEffect } from 'react';

// -------------------------------------------------------------------------- //

export const ThemeContext = createContext();

export const ThemeProvider = ({ children }) => {
    const [isDarkTheme, setIsDarkTheme] = useState(Appearance.getColorScheme() === 'dark');

    // Carga el tema guardado.
    useEffect(() => {
        const loadTheme = async () => {
            const savedTheme = await AsyncStorage.getItem('theme');

            if (savedTheme !== null) {
                setIsDarkTheme(savedTheme === 'dark');
            } else {
                setIsDarkTheme(Appearance.getColorScheme() === 'dark');
            }
        };

        loadTheme();
    }, []);

    // Guardar el tema al cambiarlo.
    useEffect(() => {
        const saveTheme = async () => {
            await AsyncStorage.setItem('theme', isDarkTheme ? 'dark' : 'light');
        };

        saveTheme();
    }, [ isDarkTheme ]);

    return (
        <ThemeContext.Provider value={{ isDarkTheme, setIsDarkTheme }}>
            {children}
        </ThemeContext.Provider>
    );
};

export const useTheme = () => useContext(ThemeContext);

// -------------------------------------------------------------------------- //