
import React, { useState } from 'react';
import { Ionicons } from '@expo/vector-icons';
import Toast from 'react-native-toast-message';
import { BASE_URL, MAIN_URL } from '../Config';
import { Estilos } from '../components/Estilos';
import { useNavigation } from '@react-navigation/native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { negroOpaco, blancoOpaco, azulOpaco } from '../components/Colores';
import { View, Image, Text, TextInput, Pressable, Platform, Alert, ActivityIndicator } from 'react-native';

const CodeWave = require('../assets/CodeWave.png');

// -------------------------------------------------------------------------- //

const ForgotPassword = () => {
    const [ matricula, setMatricula ] = useState('');
    const isButtonDisabled = matricula.trim() === '';
    const [ isLoading, setIsLoading ] = useState(false);
    const navigation = useNavigation();
    const [ usuario, setUsuario ] = useState(null);

    const defaultImage = require('../assets/defaultProfile.png');
    const profileImageURL = usuario && usuario.FotoPerfil ? { uri: `${MAIN_URL}${usuario.FotoPerfil}` } : defaultImage;

    const handleMatriculaChange = (text) => {
        const filteredText = text.replace(/[^0-9]/g, '');
        setMatricula(filteredText);
    };

    const cancelarConfirmacion = () => {
        setUsuario(null);
        setMatricula('');
    };

    const confirmarUsuario = async () => {
        setIsLoading(true);

        try {
            const response = await fetch(`${BASE_URL}/v1/solicitarCodigo/`, {
                method: 'POST', headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({ email: usuario.Email }),
            });

            const data = await response.json();
            setIsLoading(false);

            if (response.ok) {
                Toast.show({
                    type: 'success',
                    text1: 'ÉXITO',
                    text2: data.message,
                    visibilityTime: 4500,
                })
                navigation.navigate('ChangePassword', { email: usuario.Email });
                cancelarConfirmacion();
            } else {
                Toast.show({
                    type: 'error',
                    text1: 'ERROR',
                    text2: data.error,
                    visibilityTime: 4500,
                })
            }

        } catch (error) {
            Toast.show({
                type: 'error',
                text1: 'ERROR',
                text2: 'No se pudo establecer una conexión con el servidor.',
                visibilityTime: 4500,
            })
            setIsLoading(false);
        }
    };
    

    // ---------------------------------------------------------------------- //

    const fetchUsuario = async () => {
        setIsLoading(true);

        try {
            const response = await fetch(`${BASE_URL}/v1/usuario/detalles/${matricula}`, {
                method: 'GET', headers: { 'Content-Type': 'application/json' },
            });

            const data = await response.json();
            setIsLoading(false);

            if (response.ok) {
                setUsuario(data);
            } else {
                Toast.show({
                    type: 'error',
                    text1: 'ERROR',
                    text2: data.error,
                    visibilityTime: 4500,
                })
            }

        } catch (error) {
            Toast.show({
                type: 'error',
                text1: 'ERROR',
                text2: 'No se pudo establecer una conexión con el servidor.',
                visibilityTime: 4500,
            })
            setIsLoading(false);
        }
    };

    // ---------------------------------------------------------------------- //

    return (
        <SafeAreaView style={[ Estilos.safeContainer, { backgroundColor: negroOpaco } ]}>

            <View style={[ Estilos.headerContainer, { backgroundColor: azulOpaco } ]}>
                <Text style={[ Estilos.headerTitle, { color: blancoOpaco } ]}>Reestablecer contraseña</Text>

                <Pressable onPress={() => { cancelarConfirmacion(); navigation.navigate('Login'); }}>
                    <Ionicons name="close" size={24} color={ blancoOpaco } />
                </Pressable>
            </View>

            {/* ------------------------------------------------------------ */}

            <View style={ Estilos.mainContainer }>

                {usuario ? ( <>

                    <View style={Estilos.userView}>
                        <Image source={profileImageURL} style={[Estilos.profileIMG, { borderColor: azulOpaco }]} />

                        <View>
                            <Text style={Estilos.passwordButton}>¿Es usted?</Text>
                            <Text style={[ Estilos.profileName, { color: blancoOpaco } ]}>{usuario.Nombres} {usuario.ApellidoPaterno}</Text>
                        </View>
                    </View>

                    <View style={Estilos.userView}>
                        <Pressable onPress={cancelarConfirmacion} style={Estilos.cancelButton}>
                            <Text style={Estilos.loginText}>Cancelar</Text>
                        </Pressable>

                        <Pressable onPress={confirmarUsuario} disabled={isLoading} style={Estilos.confirmButton}>
                            {isLoading ? (
                                <ActivityIndicator size={18} color={blancoOpaco} />
                            ) : (
                                <Text style={Estilos.loginText}>Confirmar</Text>
                            )}
                        </Pressable>
                    </View>

                </> ) : ( <>

                    <Image source={CodeWave} style={Estilos.iconCW} />

                    <View style={Estilos.inputView}>
                        <View style={Estilos.inputIcon}>
                            <Ionicons name='school' size={18} color={blancoOpaco} />
                        </View>

                        <TextInput placeholder='Matrícula' inputMode='numeric' maxLength={10}
                            placeholderTextColor={blancoOpaco} onChangeText={handleMatriculaChange} value={matricula} 
                            style={[Estilos.textInput, Platform.OS === 'web' && {outlineStyle: 'none'}]} />
                    </View>

                    <Pressable style={isButtonDisabled ? Estilos.ButtonDisabled : Estilos.loginButton}
                    onPress={fetchUsuario} disabled={isButtonDisabled || isLoading}>
                        {isLoading ? (
                            <ActivityIndicator size={18} color={blancoOpaco} />
                        ) : (
                            <Text style={Estilos.loginText}>CONFIRMAR MATRÍCULA</Text>
                        )}
                    </Pressable>

                </> )}

            </View>

        </SafeAreaView>
    );

}

export default ForgotPassword;

// -------------------------------------------------------------------------- //