
// -------------------------------------------------------------------------- //

import { BASE_URL } from '../Config';
import React, { useState } from 'react';
import Toast from 'react-native-toast-message';
import { Estilos } from '../components/Estilos';
import { useNavigation } from '@react-navigation/native';
import { useUser } from '../components/Provider/UserProvider';
import { SafeAreaView } from 'react-native-safe-area-context';
import { blancoOpaco, negroOpaco } from '../components/Colores';
import { FontAwesome5, Entypo, Ionicons } from '@expo/vector-icons';
import { Image, View, Text, TextInput, Pressable, Platform, ActivityIndicator } from 'react-native';

const appLogo = require('../assets/AppIconDark.png');

// -------------------------------------------------------------------------- //

const IniciarSesion = () => {
    const [ password, setPassword ] = useState('');
    const [ matricula, setMatricula ] = useState('');
    const isButtonDisabled = matricula.trim() === '' || password.trim() === '';

    const [ passwordVisible, setPasswordVisible ] = useState(false);
    const [ isLoading, setIsLoading ] = useState(false);
    const { updateUser } = useUser();
    const navigation = useNavigation();

    // ---------------------------------------------------------------------- //

    const togglePasswordVisibility = () => {
        setPasswordVisible(!passwordVisible);
    };

    const handleMatriculaChange = (text) => {
        const filteredText = text.replace(/[^0-9]/g, '');
        setMatricula(filteredText);
    };

    const iniciarSesion = async () => {
        setIsLoading(true);

        try {
            const response = await fetch(`${BASE_URL}/v1/login/`, {
                method: 'POST', headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({ Matricula: matricula, Clave: password }),
            });

            const data = await response.json();
            setIsLoading(false);

            if (response.ok) {
                await updateUser(data);
                navigation.navigate('Home'); 

            } else {
                Toast.show({
                    type: 'error',
                    text1: 'ERROR',
                    text2: data.error,
                    visibilityTime: 4500,
                })
            }

        } catch (error) {
            Toast.show({
                type: 'error',
                text1: 'ERROR',
                text2: 'No se pudo establecer una conexión con el servidor.',
                visibilityTime: 4500,
            })
            setIsLoading(false);
        }
    };

    // ---------------------------------------------------------------------- //

    return (
        <SafeAreaView style={[ Estilos.mainContainer, { backgroundColor: negroOpaco } ]}>
            <Image source={appLogo} style={Estilos.iconCW} />

            <View style={Estilos.inputView}>
                <View style={Estilos.inputIcon}>
                    <Ionicons name='school' size={18} color={blancoOpaco} />
                </View>

                <TextInput placeholder='Matrícula' inputMode='numeric' maxLength={10}
                    placeholderTextColor={blancoOpaco} onChangeText={handleMatriculaChange} value={matricula} 
                    style={[Estilos.textInput, Platform.OS === 'web' && {outlineStyle: 'none'}]} />
            </View>

            <View style={Estilos.inputView}>
                <View style={Estilos.inputIcon}>
                    <Entypo name='lock' size={18} color={blancoOpaco} />
                </View>

                <TextInput placeholder='Contraseña' secureTextEntry={!passwordVisible} maxLength={32}
                    placeholderTextColor={blancoOpaco} onChangeText={setPassword} value={password}
                    style={[Estilos.textInput, Platform.OS === 'web' && {outlineStyle: 'none', width: 182 } ]} />

                <Pressable onPress={togglePasswordVisibility} style={{ marginLeft: 10 }}>
                    { passwordVisible ? <FontAwesome5 name='eye-slash' size={14} color={blancoOpaco} /> : <FontAwesome5 name='eye' size={16} color={blancoOpaco} /> }
                </Pressable>
            </View>

            <Pressable style={isButtonDisabled ? Estilos.ButtonDisabled : Estilos.loginButton}
                onPress={iniciarSesion} disabled={isButtonDisabled || isLoading}>
                {isLoading ? (
                    <ActivityIndicator size={18} color={blancoOpaco} />
                ) : (
                    <Text style={Estilos.loginText}>INICIAR SESIÓN</Text>
                )}
            </Pressable>

            <Pressable onPress={() => { navigation.navigate('ForgotPassword'); }}>
                <Text style={Estilos.passwordButton}>¿Olvidaste tu contraseña?</Text>
            </Pressable>

        </SafeAreaView>
    );

};

export default IniciarSesion;

// -------------------------------------------------------------------------- //