
import Colores from '../components/Colores';
import React, { useState, useRef } from 'react';
import OnboardingItem from '../components/OnboardingItem';
import { useUser } from '../components/Provider/UserProvider';
import { useTheme } from '../components/Provider/ThemeContext';
import { View, StyleSheet, Image, FlatList, Text, Animated, Dimensions, TouchableOpacity } from 'react-native';

// -------------------------------------------------------------------------- //

const puntitoSize = 8;
const { width } = Dimensions.get('window');

// -------------------------------------------------------------------------- //

const Inicio = () => {
    const { user } = useUser();
    const themeColor = Colores();
    const { isDarkTheme } = useTheme();

    // ---------------------------------------------------------------------- //

    const iconDark = require('../assets/AppIconDark.png');
    const iconLight = require('../assets/AppIconLight.png');
    const AppIcon = isDarkTheme ? iconDark : iconLight;

    const Slides = [

        {
            id: 1,
            title: `Bienvenido, ${user?.Nombres}`,
            description: '',
            image: AppIcon,
            actions: []
        },

        {
            id: 2,
            title: '¡Glorioso propósito!',
            description: 'Esta aplicación ofrece una manera cómoda de gestionar la renta de casilleros, garantizando una experiencia más segura para los usuarios. Proporcionando además, una experiencia personalizada y divertida que fomenta la interacción del usuario con la aplicación.',
            image: null,
            actions: [
                { icon: 'wallet', title: 'Renta de casillero (PayPal)', description: 'Renta el casillero de tu preferencia.' },
                { icon: 'lock-open', title: 'Personalización de casillero', description: 'Añade un aspecto único a tu casillero con stickers.' },
                { icon: 'user-edit', title: 'Personalización de perfil', description: 'Permite actualizar la información de tu perfi según desees.' },
            ]
        },

    ];

    // ---------------------------------------------------------------------- //

    const flatListRef = useRef(null);
    const scrollX = useRef(new Animated.Value(0)).current;
    const [ currentIndex, setCurrentIndex ] = useState(0);

    const onViewableItemsChanged = useRef(({ viewableItems }) => {
        if (viewableItems && viewableItems.length > 0) {
            setCurrentIndex(viewableItems[0].index);
        }
    }).current;

    const renderCarouselItem = ({ item }) => (
        <View style={{ width }}>
            <OnboardingItem item={item} />
        </View>
    );

    const renderDot = (index) => {
        const opacity = index === currentIndex ? 1 : 0.3;

        return (
            <TouchableOpacity key={index} onPress={() => scrollToIndex(index)}>
                <View style={[ styles.puntito, { opacity, backgroundColor: themeColor.fondoDinamico } ]} />
            </TouchableOpacity>
        );
    };

    const scrollToIndex = (index) => {
        if (flatListRef.current) {
            flatListRef.current.scrollToIndex({ animated: true, index });
        }
    };

// -------------------------------------------------------------------------- //

    return (
        <View style={[ styles.container, { backgroundColor: themeColor.textoDinamico } ]}>
            
            <FlatList
                data={Slides} renderItem={renderCarouselItem}
                horizontal showsHorizontalScrollIndicator={false}
                pagingEnabled bounces={false} keyExtractor={(item) => item.id.toString()}
                ref={flatListRef} onViewableItemsChanged={onViewableItemsChanged}
                viewabilityConfig={{ viewAreaCoveragePercentThreshold: 50 }}
                onScroll={Animated.event([{ nativeEvent: { contentOffset: { x: scrollX } } }], { useNativeDriver: false })}
                contentContainerStyle={{ flexGrow: 1 }}
            />

            <View style={styles.dotContainer}>
                { Slides.map((_, index) => renderDot(index)) }
            </View>
        </View>
    );
};

export default Inicio;

// -------------------------------------------------------------------------- //

const styles = StyleSheet.create({

    container: {
        flex: 1,
    },

    dotContainer: {
        marginBottom: 60,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },

    puntito: {
        width: puntitoSize,
        height: puntitoSize,
        marginHorizontal: 4,
        borderRadius: puntitoSize / 2,
    },

});

// -------------------------------------------------------------------------- //