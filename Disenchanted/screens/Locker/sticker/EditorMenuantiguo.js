import React, { useState } from 'react';
import { View, TouchableOpacity, Text, StyleSheet } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';

const EditorMenu = ({
  clearSelectedStickers,
  addRandomStickers,
  openStickersModal,
  toggleControllers,
}) => {
  const [isPressed, setIsPressed] = useState(false);
  const [controllersActive, setControllersActive] = useState(false); 

  const handlePressIn = () => {
    setIsPressed(true);
  };

  const handlePressOut = () => {
    setIsPressed(false);
  };

  const handleControllersPress = () => {
    toggleControllers(); 
    setControllersActive(!controllersActive); 
  };

  return (
    <View style={styles.menuContainer}>
      <TouchableOpacity
        style={[styles.menuButton, isPressed && styles.menuButtonPressed]}
        onPress={openStickersModal}
        onPressIn={handlePressIn}
        onPressOut={handlePressOut}>
        <FontAwesome name="plus-square-o" size={24} color="white"/>
        <Text style={[styles.menuButtonText, isPressed && styles.menuButtonTextPressed]}>Stickers</Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={[styles.menuButton, isPressed && styles.menuButtonPressed]}
        onPress={clearSelectedStickers}
        onPressIn={handlePressIn}
        onPressOut={handlePressOut}>
        <FontAwesome name="trash-o" size={24} color="white" />
        <Text style={[styles.menuButtonText, isPressed && styles.menuButtonTextPressed]}>Limpiar Locker</Text>
      </TouchableOpacity>

      <TouchableOpacity
            style={[
              styles.menuButton,
              isPressed && styles.menuButtonPressed,
              controllersActive && styles.menuButtonActive, 
            ]}
            onPress={handleControllersPress} 
            onPressIn={handlePressIn}
            onPressOut={handlePressOut}>
            <FontAwesome name="sliders" size={30} color="white" />
            <Text style={[styles.menuButtonText, isPressed && styles.menuButtonTextPressed]}>Controladores</Text>
          </TouchableOpacity>

      <TouchableOpacity
        style={[styles.menuButton, isPressed && styles.menuButtonPressed]}
        onPress={addRandomStickers}
        onPressIn={handlePressIn}
        onPressOut={handlePressOut}>
        <FontAwesome name="save" size={24} color="white" />
        <Text style={[styles.menuButtonText, isPressed && styles.menuButtonTextPressed]}>Guardar cambios</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  menuContainer: {
    position: 'absolute',
    borderRadius: 50,
    bottom: 0,
    flexDirection: 'row',
    justifyContent: 'space-around',
    width: '90%',
    backgroundColor: 'rgba(0, 0, 0, 0.3)',
    paddingVertical: 10,
  },
  menuButton: {
    paddingHorizontal: 20,
    alignItems: 'center',
  },
  menuButtonPressed: {
    backgroundColor: 'rgba(255, 255, 255, 0.2)', 
    borderCurve: 20,
  },
  menuButtonText: {
    color: 'white',
    fontSize: 16,
    marginTop: 5,
  },
  menuButtonTextPressed: {
    color: 'gray', 
  },
  menuButtonPressed: {
    backgroundColor: 'rgba(255, 255, 255, 0.2)', 
  },

  menuButtonActive: {
    backgroundColor: 'red', 
  },

});

export default EditorMenu;