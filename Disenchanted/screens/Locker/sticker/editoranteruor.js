import React, { useState, useRef } from 'react';
import { View, Image, TouchableOpacity, Text, StyleSheet, Modal, ScrollView, PanResponder } from 'react-native';
import * as Stickers from './Images';
import { AntDesign } from '@expo/vector-icons';

const MAX_STICKERS = 10;

const {
  Cinnamon,
  girl1,
  girl2,
  kirby,
  locker2,
  Michi,
  Iseeu,
  Axolot,
  Grass,
  Hasbi,
  Miku1,
  Miku2,
  Konosuba,
  Gonzalo,
  Coca,
  samus,
  Zelda,
  CaraMenso,
  Moai,
  girl3,
  girl4,
  dog1,
  dog2,
  dog3,
  abrazo,
  dog6,
  girl5
} = Stickers;

import EditorMenu from './EditorMenu';

const Editor = () => {
  const [selectedStickers, setSelectedStickers] = useState([]);
  const [showStickersModal, setShowStickersModal] = useState(false);
  const [scrollEnabled, setScrollEnabled] = useState(true);

  const Set_sticker = (stickerUri) => {
    if (selectedStickers.length < MAX_STICKERS) {
      const newSticker = { uri: stickerUri, position: { x: 0, y: 0 }, size: 210 };
      setSelectedStickers([...selectedStickers, newSticker]);
      closeStickersModal(true);
    }else{
      console.log("Se ha utilizado el maximo de stickers permitidos.");
    }
  };

  const removeSticker = (stickerIndex) => {
    const updatedStickers = [...selectedStickers];
    updatedStickers.splice(stickerIndex, 1);
    setSelectedStickers(updatedStickers);
  };

  const clearSelectedStickers = () => {
    setSelectedStickers([]);
  };

  const handlePanResponderMove = (index, _, { dx, dy }) => {
    const updatedStickers = [...selectedStickers];
    updatedStickers[index].position.x += dx;
    updatedStickers[index].position.y += dy;
    setSelectedStickers(updatedStickers);
  };

  const increaseStickerSize = (stickerIndex) => {
    const updatedStickers = [...selectedStickers];
    if(updatedStickers[stickerIndex].size <500){
    updatedStickers[stickerIndex].size += 10;
    setSelectedStickers(updatedStickers);
    }
  };

  const decreaseStickerSize = (stickerIndex) => {
    const updatedStickers = [...selectedStickers];
    if (updatedStickers[stickerIndex].size > 100) {
      updatedStickers[stickerIndex].size -= 10;
      setSelectedStickers(updatedStickers);
    }
  };

  const stickers = selectedStickers.map((sticker, index) => {
    const panResponder = PanResponder.create({
      onStartShouldSetPanResponder: () => true,
      onPanResponderMove: (evt, gestureState) => handlePanResponderMove(index, evt, gestureState),
    });

    return (
      <View
        key={index}
        style={[styles.stickerContainer, { left: sticker.position.x, top: sticker.position.y }]}
        {...panResponder.panHandlers}>

        <Image source={sticker.uri} style={[styles.stickerImage, { width: sticker.size, height: sticker.size }]} />

        <TouchableOpacity style={styles.removeButton} onPress={() => removeSticker(index)}>
          <Text style={styles.removeButtonText}>X</Text>
        </TouchableOpacity>

        <TouchableOpacity style={[styles.sizeButton, styles.increaseButton]} onPress={() => increaseStickerSize(index)}>
          <Text style={styles.sizeButtonText}>+</Text>
        </TouchableOpacity>

        <TouchableOpacity style={[styles.sizeButton, styles.decreaseButton]} onPress={() => decreaseStickerSize(index)}>
          <Text style={styles.sizeButtonText}>-</Text>
        </TouchableOpacity>
      </View>
    );
  });

  const openStickersModal = () => {
    setShowStickersModal(true);
  };

  const closeStickersModal = () => {
    setShowStickersModal(false);
  };

  return (
     <View style={styles.container}>
    <Image source={locker2} style={styles.backgroundImage} />
    {stickers}
    <EditorMenu
      clearSelectedStickers={clearSelectedStickers}
      Set_sticker={Set_sticker}
      openStickersModal={() => setShowStickersModal(true)}
    />
    <Modal visible={showStickersModal} animationType="fade" transparent={true}>
      <View style={styles.modalBackground}>

        <View style={styles.cerrarmodalstickers}>
 <TouchableOpacity onPress={closeStickersModal}>
        <AntDesign name="caretdown" size={90} color="white" />
            </TouchableOpacity>
        </View>
     
        <View style={styles.stickersModalContainer}>

            <View style={styles.containertextmodal}>
            <Text style={styles.modalTitle}>Selecciona un Sticker de preferencia para su casillero.</Text>
            </View>

            <ScrollView vertical style={styles.stickersModal}>
            
            <View style={styles.stickerRow}>

               <TouchableOpacity onPress={() => Set_sticker(Cinnamon)}>
                <Image source={Cinnamon} style={[styles.stickerImage, styles.modalSticker]} />
              </TouchableOpacity>
              
              <TouchableOpacity onPress={() => Set_sticker(girl1)}>
                <Image source={girl1} style={[styles.stickerImage, styles.modalSticker]} />
              </TouchableOpacity>
              
              <TouchableOpacity onPress={() => Set_sticker(girl2)}>
                <Image source={girl2} style={[styles.stickerImage, styles.modalSticker]} />
              </TouchableOpacity>
              
              <TouchableOpacity onPress={() => Set_sticker(kirby)}>
                <Image source={kirby} style={[styles.stickerImage, styles.modalSticker]} />
              </TouchableOpacity>
              
              <TouchableOpacity onPress={() => Set_sticker(Gonzalo)}>
                <Image source={Gonzalo} style={[styles.stickerImage, styles.modalSticker]} />
              </TouchableOpacity>

            </View>

            <View style={styles.stickerRow}>

              <TouchableOpacity onPress={() => Set_sticker(Michi)}>
                <Image source={Michi} style={[styles.stickerImage, styles.modalSticker]} />
              </TouchableOpacity>

              <TouchableOpacity onPress={() => Set_sticker(Iseeu)}>
                <Image source={Iseeu} style={[styles.stickerImage, styles.modalSticker]} />
              </TouchableOpacity>

              <TouchableOpacity onPress={() => Set_sticker(Axolot)}>
                <Image source={Axolot} style={[styles.stickerImage, styles.modalSticker]} />
              </TouchableOpacity>

              <TouchableOpacity onPress={() => Set_sticker(Grass)}>
                <Image source={Grass} style={[styles.stickerImage, styles.modalSticker]} />
              </TouchableOpacity>

              <TouchableOpacity onPress={() => Set_sticker(Hasbi)}>
                <Image source={Hasbi} style={[styles.stickerImage, styles.modalSticker]} />
              </TouchableOpacity>

              </View>
                <View style={styles.stickerRow}>
              <TouchableOpacity onPress={() => Set_sticker(Miku1)}>
                <Image source={Miku1} style={[styles.stickerImage, styles.modalSticker]} />
              </TouchableOpacity>

              <TouchableOpacity onPress={() => Set_sticker(Miku2)}>
                <Image source={Miku2} style={[styles.stickerImage, styles.modalSticker]} />
              </TouchableOpacity>

              <TouchableOpacity onPress={() => Set_sticker(Konosuba)}>
                <Image source={Konosuba} style={[styles.stickerImage, styles.modalSticker]} />
              </TouchableOpacity>

              <TouchableOpacity onPress={() => Set_sticker(Coca)}>
                <Image source={Coca} style={[styles.stickerImage, styles.modalSticker]} />
              </TouchableOpacity>

              <TouchableOpacity onPress={() => Set_sticker(samus)}>
                <Image source={samus} style={[styles.stickerImage, styles.modalSticker]} />
              </TouchableOpacity>

              </View>
              <View style={styles.stickerRow}>
              <TouchableOpacity onPress={() => Set_sticker(Zelda)}>
                <Image source={Zelda} style={[styles.stickerImage, styles.modalSticker]} />
              </TouchableOpacity>

              <TouchableOpacity onPress={() => Set_sticker(CaraMenso)}>
                <Image source={CaraMenso} style={[styles.stickerImage, styles.modalSticker]} />
              </TouchableOpacity>

              <TouchableOpacity onPress={() => Set_sticker(Moai)}>
                <Image source={Moai} style={[styles.stickerImage, styles.modalSticker]} />
              </TouchableOpacity>

              <TouchableOpacity onPress={() => Set_sticker(girl3)}>
                <Image source={girl3} style={[styles.stickerImage, styles.modalSticker]} />
              </TouchableOpacity>

              <TouchableOpacity onPress={() => Set_sticker(girl4)}>
                <Image source={girl4} style={[styles.stickerImage, styles.modalSticker]} />
              </TouchableOpacity>
              </View>

              <View style={styles.stickerRow}>
              <TouchableOpacity onPress={() => Set_sticker(dog1)}>
                <Image source={dog1} style={[styles.stickerImage, styles.modalSticker]} />
              </TouchableOpacity>

              <TouchableOpacity onPress={() => Set_sticker(dog2)}>
                <Image source={dog2} style={[styles.stickerImage, styles.modalSticker]} />
              </TouchableOpacity>

              <TouchableOpacity onPress={() => Set_sticker(dog3)}>
                <Image source={dog3} style={[styles.stickerImage, styles.modalSticker]} />
              </TouchableOpacity>

              <TouchableOpacity onPress={() => Set_sticker(dog6)}>
                <Image source={dog6} style={[styles.stickerImage, styles.modalSticker]} />
              </TouchableOpacity>

              <TouchableOpacity onPress={() => Set_sticker(abrazo)}>
                <Image source={abrazo} style={[styles.stickerImage, styles.modalSticker]} />
              </TouchableOpacity>

              </View>

              <View style={styles.stickerRow}>
              <TouchableOpacity onPress={() => Set_sticker(girl5)}>
                <Image source={girl5} style={[styles.stickerImage, styles.modalSticker]} />
              </TouchableOpacity>
              </View>

            </ScrollView>
        </View>
      </View>
    </Modal>
  </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },

  backgroundImage: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    resizeMode: 'contain',
  },
  stickerContainer: {
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
  },

  stickerImage: {
    width: '100%',
    height: '100%',
    resizeMode: 'contain',
  },

  modalSticker: {
    width: 100,
    height: 100,
    resizeMode: 'contain',
  },
  
  removeButton: {
    position: 'absolute',
    top: 0,
    right: 0,
    backgroundColor: 'rgba(255, 0, 0, 0.5)',
    borderRadius: 10,
    width: 20,
    height: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },

  removeButtonText: {
    color: 'white',
    fontWeight: 'bold',
  },

  sizeButton: {
    position: 'absolute',
    width: 30,
    height: 30,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },

  increaseButton: {
    backgroundColor: 'rgba(0, 0, 255, 0.5)',
    bottom: 0,
    left: 0,
  },
  decreaseButton: {
    backgroundColor: 'rgba(255, 0, 0, 0.5)',
    bottom: 0,
    left: 25,
  },
  sizeButtonText: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 16,
  },
  modalBackground: {
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.7)', 
    justifyContent: 'center',
    alignItems: 'center',
  },
  stickersModalContainer: {
    //backgroundColor: 'white',
    backgroundColor: 'rgba(0, 0, 0, 0.4)', 
    borderRadius: 10,
    padding: 20,
    width: '80%',

  },
  stickersModal: {
    marginTop: 20,
  },

  modalTitle: {
    alignSelf: 'center',
    fontSize: 24,
    marginBottom: 20,
    marginTop: 20,
    color: 'white',
    font: 'fantasy',

  },


  containertextmodal: {
    borderRadius: 20,
    backgroundColor: 'rgba(240, 240, 240, 0.4)', 
    width: '100%',
  },


  cerrarmodalstickers: {
    width: '10%',
    alignItems: 'center',
    justifyContent: 'center',
    
  },
  
  modalSticker: {
    width: 150,
    height: 150,
    resizeMode: 'contain',
  },
  
  stickerRow: {
  flexDirection: 'row',
  justifyContent: 'space-between',
  marginBottom: 10, 
},

closeStickersModalButtonText: {
  color: 'white',
  fontSize: 16,
  alignSelf: 'center',
},

});

export default Editor;

