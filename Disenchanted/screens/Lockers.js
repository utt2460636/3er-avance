
import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Modal, WebView, TouchableOpacity } from 'react-native';

export default class Lockers extends React.Component{
  state = {
    showModal: false,
    status: 'Pending'
  }

  render(){
    return(
      <View style={{marginTop: 100}}>
        <Modal visible={this.state.showModal}
        onRequestClose={() => this.setState({showModal: false})}>
          <WebView source={{ uri: 'http://localhost:3000' }}/>
        </Modal>
        <TouchableOpacity style={{width: 300, height: 100}}>
          <Text>Pay with PayPal</Text>
        </TouchableOpacity>
        <Text>Payment status: {this.state.status}</Text>
      </View>
    )
  }
} 

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    padding: 20,
    backgroundColor: '#f0f0f0',
  },
  lockerContainer: {
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 20,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  title: {
    fontSize: 22,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  detailContainer: {
    marginBottom: 10,
  },
  detailTitle: {
    fontWeight: 'bold',
  },
  detailText: {
    fontSize: 16,
  },
  paymentContainer: {
    margin: 12,
  }
});


