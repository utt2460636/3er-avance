
import React from 'react';
import { MAIN_URL } from '../../Config';
import { useNavigation } from '@react-navigation/native';
import { useUser } from '../../components/Provider/UserProvider';
import { azulOpaco, negroOpaco, blancoOpaco } from '../../components/Colores';
import { View, Text, TouchableOpacity, ImageBackground, StyleSheet } from 'react-native';

// -------------------------------------------------------------------------- //

const EditarPerfil = () => {
    const { user } = useUser();
    const navigation = useNavigation();

    const defaultBackground = require('../../assets/defaultCover.jpg');
    const coverImageUrl = user?.FotoPortada ? `${MAIN_URL}${user.FotoPortada}` : defaultBackground;

    // ---------------------------------------------------------------------- //

    const formatDateTime = (dateTimeString) => {
        const options = { year: 'numeric', month: 'long', day: 'numeric' };
        return new Date(dateTimeString).toLocaleDateString('es-ES', options);
    };

    const formatDateOnly = (dateString) => {
        const options = { year: 'numeric', month: 'long', day: 'numeric', timeZone: 'UTC' };
        const date = new Date(dateString + 'T00:00:00Z');
        return date.toLocaleDateString('es-ES', options);
    };

    return (
        <ImageBackground source={{ uri: coverImageUrl }} resizeMode='cover' style={styles.scrollView}>
            <View style={styles.darkOverlay}>

                <View style={styles.container}>
                    <View style={styles.infoDetail}>
                        <Text style={styles.infoTitle}>Correo</Text>
                        <Text style={styles.detailText}>: {user?.Email}</Text>
                    </View>

                    <View style={styles.infoDetail}>
                        <Text style={styles.infoTitle}>Número</Text>
                        <Text style={styles.detailText}>: {user?.NumTel}</Text>
                    </View>

                    <View style={styles.infoDetail}>
                        <Text style={styles.infoTitle}>Cumpleaños</Text>
                        <Text style={styles.detailText}>: {formatDateOnly(user?.Natalicio)}</Text>
                    </View>

                    <View style={styles.infoDetail}>
                        <Text style={styles.infoTitle}>Registro</Text>
                        <Text style={styles.detailText}>: {formatDateTime(user?.FechaRegistro)}</Text>
                    </View>

                    <TouchableOpacity onPress={() => { navigation.navigate('Profile') }} style={styles.editButton}>
                        <Text style={styles.editButtonText}>Regresar</Text>
                    </TouchableOpacity>
                </View>

            </View>
        </ImageBackground>
    );
};

// -------------------------------------------------------------------------- //

const styles = StyleSheet.create({

    scrollView: {
        flex: 1,
    },

    darkOverlay: {
        position: 'absolute',
        top: 0, bottom: 0, left: 0, right: 0,
        backgroundColor: 'rgba(28, 28, 30, 0.8)',
    },

    container: {
        width: 390,
        marginTop: 140,
        marginBottom: 20,
        alignSelf: 'center',
    },

    infoDetail: {
        paddingVertical: 16,
        alignItems: 'center',
        flexDirection: 'row',
    },

    infoTitle: {
        fontSize: 16,
        marginLeft: 6,
        fontWeight: 'bold',
        color: blancoOpaco,
        textAlignVertical: 'center',
    },

    detailText: {
        fontSize: 16,
        color: blancoOpaco,
    },

    editButton: {
        width: 390,
        marginTop: 12,
        borderRadius: 30,
        paddingVertical: 14,
        alignSelf: 'center',
        backgroundColor: negroOpaco,
    },

    editButtonText: {
        fontSize: 16,
        textAlign: 'center',
        color: blancoOpaco,
    },

});

export default EditarPerfil;

// -------------------------------------------------------------------------- //