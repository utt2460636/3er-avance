
import { MAIN_URL } from "../../Config";
import Colores from "../../components/Colores";
import React, { useEffect, useState } from "react";
import { Estilos } from "../../components/Estilos";
import { blancoOpaco } from "../../components/Colores";
import { AntDesign, Entypo } from '@expo/vector-icons';
import { useNavigation } from "@react-navigation/native";
import { useUser } from "../../components/Provider/UserProvider";
import { ImageBackground, ScrollView, View, Image, Text, TouchableOpacity, StatusBar, Platform } from "react-native";

// -------------------------------------------------------------------------- //

const MiPerfil = () => {
    const { user } = useUser();
    const themeColor = Colores();
    const navigation = useNavigation();
    const [ updateKey, setUpdateKey ] = useState(Date.now());

    const noPortada = require('../../assets/defaultCover.jpg');
    const noProfile = require('../../assets/defaultProfile.png');
    const ProfileURL = user && user.FotoPerfil ? { uri: `${MAIN_URL}${user.FotoPerfil}?v=${updateKey}` } : noProfile;
    const PortadaURL = user && user.FotoPortada ? { uri: `${MAIN_URL}${user.FotoPortada}?v=${updateKey}` } : noPortada;

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            setUpdateKey(Date.now());
        });

        return unsubscribe;
    }, [navigation]);


    // ---------------------------------------------------------------------- //

    return (
        <View style={Estilos.safeContainer}>
            {Platform.OS === "android" && <StatusBar translucent backgroundColor="transparent" /> }

            <ImageBackground key={updateKey} source={PortadaURL} resizeMode="cover" style={Estilos.safeContainer}>
                <View style={Estilos.darkOverlay}>
                    <ScrollView style={Estilos.safeContainer}>

                        <View style={Estilos.profileHeader}>
                            <Image key={updateKey} source={ProfileURL} style={Estilos.userImage} />

                            <View style={Estilos.profileInfo}>
                                <Text style={Estilos.userName}>{user?.Nombres} {user?.ApellidoPaterno}</Text>

                                <View style={[ Estilos.levelContainer, { backgroundColor: themeColor.fondoDinamico } ]}>
                                    <Text style={[ Estilos.level, { color: themeColor.itemDinamico } ]}>{user?.Ocupacion}</Text>
                                </View>
                            </View>

                        </View>

                        <View style={Estilos.profileSection}>

                                <View style={Estilos.aboutMe}>
                                    <Entypo name="info" size={16} color={blancoOpaco} style={Estilos.icon} />
                                    <Text style={Estilos.titleAbout}>Sobre mí</Text>
                                </View>

                                <Text style={Estilos.aboutBio}>“{user?.Biografia}”.</Text>

                            </View>

                            <TouchableOpacity style={[ Estilos.buttonDetail, { backgroundColor: themeColor.fondoDinamico} ]}
                                onPress={() => navigation.navigate('EditProfile')} >
                                
                                <View style={[Estilos.outView,
                                    {
                                        flex: 1,
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                    }
                                ]}>
                                    <AntDesign name="edit" size={14} style={{ marginRight: 8 }} color={themeColor.itemDinamico} />
                                    <Text style={[ Estilos.buttonText, { color: themeColor.itemDinamico } ]}>Detalles personales</Text>
                                </View>

                            </TouchableOpacity>

                    </ScrollView>
                </View>
            </ImageBackground>
        </View>
    );

}

export default MiPerfil;

// -------------------------------------------------------------------------- //
