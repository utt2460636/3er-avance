
from django.db import models
from django.utils import timezone
from datetime import datetime, timedelta

# ---------------------------------------------------------------------------- #

class CodigoVerificacion(models.Model):
    Email = models.EmailField()
    Codigo = models.CharField(max_length=6)
    FechaCreacion = models.DateTimeField(auto_now_add=True)

    def esValido(self):
        return timezone.now() - self.FechaCreacion <= timedelta(minutes=4)

# ---------------------------------------------------------------------------- #