
from Core.models import *
from rest_framework import serializers

# ---------------------------------------------------------------------------- #

class CarrerasSerializer(serializers.ModelSerializer):
    class Meta:
        model = Carreras
        fields = ( 'nombre' )


class GruposSerializer(serializers.ModelSerializer):
    class Meta:
        model = Grupos
        fields = '__all__'


class UsuariosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Usuarios
        fields = '__all__'


class CasillerosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Casilleros
        fields = '__all__'


class RentasSerializer(serializers.ModelSerializer):
    class Meta:
        model = Rentas
        fields = '__all__'

# ---------------------------------------------------------------------------- #

class UsuarioDetallesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Usuarios
        exclude = [ 'id', 'Clave' ]


class UsuarioCasilleroSerializer(serializers.ModelSerializer):
    class Meta:
        model = UsuarioCasillero
        exclude = [ 'id' ]

# ---------------------------------------------------------------------------- #
