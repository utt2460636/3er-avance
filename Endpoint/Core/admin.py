
from Core import models
from django.contrib import admin

# ---------------------------------------------------------------------------- #

admin.site.register(models.Carreras)
admin.site.register(models.Grupos)
admin.site.register(models.Usuarios)
admin.site.register(models.Casilleros)
admin.site.register(models.Rentas)
admin.site.register(models.UsuarioCasillero)

# ---------------------------------------------------------------------------- #