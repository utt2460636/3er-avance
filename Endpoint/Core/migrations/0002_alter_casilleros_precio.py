# Generated by Django 5.0.4 on 2024-04-07 05:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Core', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='casilleros',
            name='Precio',
            field=models.DecimalField(choices=[(120.0, '$120'), (80.0, '$80')], decimal_places=2, max_digits=6),
        ),
    ]
