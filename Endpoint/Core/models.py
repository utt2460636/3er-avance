
import datetime
from django.db import models
from django.core.exceptions import ValidationError
from django.core.validators import MaxValueValidator
from django.utils.translation import gettext_lazy as _
from django.core.validators import FileExtensionValidator
from django.contrib.auth.hashers import make_password, check_password, is_password_usable

# ---------------------------------------------------------------------------- #

class Carreras(models.Model):

    carrerasList = {
        "Contaduría": "Contaduría",
        "Desarrollo & Gestión de Software": "Desarrollo & Gestión de Software",
        "Electromecánica Industrial": "Electromecánica Industrial",
        "Energías Renovables": "Energías Renovables",
        "Entornos Virtuales & Negocios Digitales": "Entornos Virtuales & Negocios Digitales",
        "Gestión de Redes Logísticas": "Gestión de Redes Logísticas",
        "Innovación de Negocios & Mercadotecnia": "Innovación de Negocios & Mercadotecnia",
        "Logística Comercial Global": "Logística Comercial Global",
        "Manufactura Aeronáutica": "Manufactura Aeronáutica",
        "Mecatronica": "Mecatronica",
        "Procesos & Operaciones Industriales": "Procesos & Operaciones Industriales",
        "Redes Inteligentes & Ciberseguridad": "Redes Inteligentes & Ciberseguridad",
        "Tecnología Ambiental": "Tecnología Ambiental",
    }

    Nombre = models.CharField(max_length=64, null=False, choices=carrerasList)

    def __str__(self):
        return self.Nombre

# ---------------------------------------------------------------------------- #

class Grupos(models.Model):

    gradoList = {
        "1ro": "1er Grado",
        "2do": "2do Grado",
        "3ro": "3er Grado",
        "4to": "4to Grado",
        "5to": "5to Grado",
        "6to": "6to Grado",
        "7mo": "7mo Grado",
        "8vo": "8vo Grado",
        "9no": "9no Grado",
    }

    Grado = models.CharField(max_length=16, null=False, choices=gradoList)

    grupoList = {
        "A": "Grupo A",
        "B": "Grupo B",
        "C": "Grupo C",
        "D": "Grupo D",
        "E": "Grupo E",
    }

    Grupo = models.CharField(max_length=8, null=False, choices=grupoList)

    def __str__(self):
        return f"{self.Grado} {self.Grupo}"

# ---------------------------------------------------------------------------- #

def validacionEdad(value):
    FechaActual = datetime.date.today()
    DiferenciaEdad = FechaActual - datetime.timedelta(days=18*365.25)

    if isinstance(value, datetime.datetime):
        value = value.date()

    if value > DiferenciaEdad:
        raise ValidationError(_('Debe poseer un mínimo de 18 años para poder registrarse.'), params={'value': value})

class Usuarios(models.Model):
    Matricula = models.CharField(max_length=10, null=False, unique=True)
    Clave = models.CharField(max_length=128, null=False)

    # ------------------------------------------------------------------------ #

    Nombres = models.CharField(max_length=128, null=False)
    ApellidoPaterno = models.CharField(max_length=64, null=False)
    ApellidoMaterno = models.CharField(max_length=64, null=True, blank=True)

    genderList = {
        "Masculino": "Masculino",
        "Femenino": "Femenino",
    }

    Genero = models.CharField(max_length=32, null=False, choices=genderList)
    Natalicio = models.DateField(null=False, validators=[validacionEdad])
    NumTel = models.CharField(default="(666) 666-6666", max_length=16)
    Email = models.CharField(max_length=128, null=False, unique=True)

    # ------------------------------------------------------------------------ #

    ocupationList = {
        "Alumno": "Alumno",
        "Docente": "Docente",
        "Administrativo": "Administrativo",
    }

    Ocupacion = models.CharField(max_length=32, null=False, choices=ocupationList)
    FechaRegistro = models.DateTimeField(auto_now_add=True, auto_now=False)

    FotoPerfil = models.ImageField(upload_to='usuarios/perfil/', null=True, blank=True, validators=[FileExtensionValidator(['jpg', 'jpeg', 'png', 'webp'])])
    FotoPortada = models.ImageField(upload_to='usuarios/portada/', null=True, blank=True, validators=[FileExtensionValidator(['jpg', 'jpeg', 'png', 'webp'])])
    Biografia = models.CharField(max_length=512, null=True, blank=True)

    def save(self, *args, **kwargs):
        if not self.pk or not is_password_usable(self.Clave):
            self.Clave = make_password(self.Clave)
        super(Usuarios, self).save(*args, **kwargs)

    def __str__(self):
        return f"{self.Nombres} {self.ApellidoPaterno} {self.ApellidoMaterno}"    

# ---------------------------------------------------------------------------- #

class Casilleros(models.Model):
    Disponibilidad = models.BooleanField(default=True, null=False)

    sizeList = {
        "Normal": "Normal",
        "Grande": "Grande",
    }

    Tamano = models.CharField(max_length=8, choices=sizeList, null=False)
    
    precioList = {
        (80.00, "$80"),
        (120.00, "$120"),
    }
    
    Precio = models.DecimalField(max_digits=6, decimal_places=2, choices=precioList, null=False)

    docenciaList = {
        "Docencia 1": "Docencia 1",
        "Docencia 2": "Docencia 2",
        "Docencia 3": "Docencia 3",
        "Docencia 4": "Docencia 4",
        "Docencia 5": "Docencia 5",
    }

    Docencia = models.CharField(max_length=16, choices=docenciaList, null=False)

    pisoList = {
        "Planta Baja": "Planta Baja",
        "Planta Alta": "Planta Alta",
    }
    
    Piso = models.CharField(max_length=16, choices=pisoList, null=False)

    def __str__(self):
        return str(self.pk)

# ---------------------------------------------------------------------------- #

class Rentas(models.Model):
    # Referencia = models.CharField(max_length=16, null=False)
    RentaInicio = models.DateTimeField(auto_now_add=True, auto_now=False)
    RentaFinal = models.DateTimeField(auto_now_add=True, auto_now=False)
    FechaPago = models.DateTimeField(auto_now_add=True, auto_now=False)

    Usuario = models.ForeignKey(Usuarios, on_delete=models.CASCADE, null=False)
    Casillero = models.ForeignKey(Casilleros, on_delete=models.CASCADE, null=False)

    # periodoList = {
    #     "Enero - Abril": "Enero - Abril",
    #     "Mayo - Agosto": "Mayo - Agosto",
    #     "Septiembre - Diciembre": "Septiembre - Diciembre",
    # }

    # Periodo = models.CharField(max_length=40, choices=periodoList)

    def __str__(self):
        return self.id

# ---------------------------------------------------------------------------- #

# import jsonfield

class UsuarioCasillero(models.Model):
    Usuario = models.ForeignKey(Usuarios, on_delete=models.CASCADE, null=False)
    Casillero = models.ForeignKey(Casilleros, on_delete=models.CASCADE, null=False)
    Stickers = models.JSONField()

# ---------------------------------------------------------------------------- #
